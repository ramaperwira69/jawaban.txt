cd C:\xampp\mysql\bin
cmd: mysql -uroot

1. Membuat database
creat database myshop

2. Membuat table di dalam database
TABLE USERS
create table users(
    -> id int(8) auto_increment primary key,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );

TABLE ITEMS
create table items(
    -> id int(8) auto_increment,
    -> name varchar(255),
    -> description varchar(255),
    -> price int(8),
    -> stock int(8),
    -> category_id int (8),
    -> primary key(id),
    -> foreign key(category_id) references categories(id)
    -> );

TABLE CATEGORIES
create table categories(
    -> id int(8) auto_increment primary key,
    -> name varchar(255)
    -> );

3. Memasukkan data pada tabel

CATEGORIES 
insert into categories(name) values("gadget"),("cloth"),("men"),("women"),("branded");
select * from categories;

USERS
insert into users(name, email, password) values("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");
select * from users;

ITEMS
insert into items(name, description, price, stock, category_id) values("Sumsang b50", "hape keren dari merek sumsang", 4000000, 100, 1),("Uniklooh", "baju keren dari brand ternama", 500000, 50, 2),("IMHO Watch", "jam tangan anak yang jujur banget", 2000000, 10, 1);
select * from items;

4. Mengambil data dari database

a. mengambil data users
select name, email from users;

b. mengambil data items
select * from items where price > 1000000;
select * from items where name like "%Watch%";

c. menampilkan data items join dengan kategori
select items.id, items.name, items.description, items.price, items.stock, items.category_id, categories.name from items inner join categories on items.category_id = categories.id;

5. Mengubah data dari database
update items set price = 2500000 where id = 1;

